% Virtual University: Integration-Manager based Git Workflow
% Jesse Jaara <jaara.jesse.j@student.uta.fi>

# Working with git

This document helps one to set up and use a git working environment,
that gives the highest priority for the integrity of core repository.
As a drawback the git workflow is a tad more complex compared to a
regular one person project.

The workflow style assumed in this document is called
*Integrtation-Manager Workflow*. In this workflow only one member of
the team has full access to the official repository and all changes to
code must be first approved by the repository manager. For more
information on *Integration-manager Workflow* and other types of Git
workflows checkout the [chapter 5.1][c51] in Git Pro.
[c51]: https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows

This document assumes one to use the official command line git client,
although there is nothing preventing one from using a graphical client
if one so wishes, providing it meets all of the required
functionality. All of the git commands should map directly to buttons
and menus on most of the graphical clients.

To learn all there is to learn about using Git, please read the great
and free book [*Pro Git*](https://git-scm.com/book/en/v2) by Scott
Chason and Ben Straub.


## What is Git?

Git is a modern decentralized version control management system. The
version history in Git is made out of commits. Every commit has one or
more parents, meaning that the changes included the in the commit
reflect the change in the state of the repository between the commit
and it's parents. Typically a git commit only has one parent, which
the optimal situation one should try to aim for. The newest commit is
called the **HEAD**.

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 2em]{
Commit 1 -- Commit 2 -- Commit 3 -- Commit 4 -- Commit 5
};
\gitHEAD
{above=of Commit 5}
{Commit 5}
\end{tikzpicture}
\caption{Simple git commit history}
\end{figure}

One of the most useful features of Git are the branches, a branch
allows you to work on the code in a controlled fashion. You can branch
out of your main branch to work on new features, while retaining a
fully functional copy of your code in your main branch. When the time
comes and your feature is ready you can then merge it to be a part of
the master branch, that is it becomes part of the main version
history. The **HEAD** always refers to the newest commit in the branch
you are working on.

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 2em]{
Commit 1 -- Commit 2 -- Commit 3 -- {
    Commit 6 -- Commit 7,
    Commit 4 -- Commit 5
}};
\gitHEAD
{below=of Commit 5}
{Commit 5}
\end{tikzpicture}
\caption{Simple git repository with a branch on it}
\end{figure}


\clearpage

## Preparing a private working clone of the repository

The following links can be used to obtain additional information about
the commands that are used in this chapter.

* [Git clone manual page](https://git-scm.com/docs/git-clone)
* [Git remote manual page](https://git-scm.com/docs/git-remote)


1. Go to the Virtual University
   [team page](https://bitbucket.org/virtualuniversity/) in Bitbucket.

2. Open the repository you want to start working on (__IV360-Edit__).

3. Create a fork of the repository, action icon at the left hand
   panel. You can disable _wiki_ and _issue tracker_, but other
   default settings are OK.

4.  Clone your private repository to your computer, it is recommended to
    use the __SSH__ URL instead of the HTTPS URL, as this allows one to
    use the secure public key authentication method. You can find the
    cloning url at right top-corner of your repository's page.

    ~~~~~ {#preparing-repo-clone .bash}
    # Go to your school or coding projects folder
    cd my/projects/VirtualUniversity/

    # Clone your private copy of the repository to your local machine
    git clone git@bitbucket.org:YourUsername/iv360-edit.git IV360-Edit

    # You should see something similar to
    # Cloning into 'IV360-Edit'...
    # remote: Counting objects: 21, done.
    # remote: Compressing objects: 100% (18/18), done.
    # remote: Total 21 (delta 1), reused 0 (delta 0)
    # Receiving objects: 100% (21/21), 96.37 KiB | 0 bytes/s, done.
    # Resolving deltas: 100% (1/1), done.
    # Checking connectivity... done.

    # Add the official version of the repository as an alternative
    # remote.
    git remote add official git@bitbucket.org:virtualuniversity/iv360-edit.git
    ~~~~~


## Creating a new branch to work on

For more detail information about branching and related topics refer
to the following links:

* [Pro Git Branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
* [Git branch manual page](https://git-scm.com/docs/git-branch)
* [Git checkout manual page](https://git-scm.com/docs/git-checkout)
* [Git pull manual page](https://git-scm.com/docs/git-pull)

Before one starts committing, that is contributing new code to the
project's repository it is important to prepare a clean environment to
work on. By always using a branch when you start working on your code,
you can ensure that you always have clean copy available in your local
__master__ branch.


1.  When you create a branch it is always based on the current HEAD of
    whatever branch is currently checked out. Usually one wants to
    base the branch on the __master__ branch. So first checkout it if it
    isn't already:

    ~~~~ {.bash}
    git checkout master
    ~~~~

2.  Next make sure you are fully up-to-date with the official version
    of our code base:

    ~~~~ {.bash}
    git pull official master
    ~~~~

    If you have kept your master branch clean and have done all work
    in external branches the command should finish clean and
    successfully. If you have committed something to the __master__
    then the command will either fail due to conflict, or you pollute
    the version history, preventing a clean merging between your yet
    to be new feature-branch and the __official/master__ branch.

3.  Now you are ready to actually create a new branch. For example to
    create a branch called *CssCleanup* you would do:

    ~~~~ {.bash}
    git branch CssCleanup
    ~~~~

    Remember to give good descriptive names to your branches, the name
    should not be too long, only a few words. If your branch is
    primarily for fixing a specific bug in the bug tracker, you might
    want to consider naming the the branch after the bug number, for
    example *Fixes_bug_456*.

4.  The only thing left is to checkout the branch and start working on
    it.

    ~~~~ {.bash}
    git checkout CssCleanup
    ~~~~

    You are now ready to start working in the branch.

\clearpage

## Committing changes

Links to external resources concerning the topic:

* [Git Pro – 2.2 Git Basics - Recording changes to the repository](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)
* [Git status manual page](https://git-scm.com/docs/git-status)
* [Git diff manual page](https://git-scm.com/docs/git-diff)
* [Git commit manual page](https://git-scm.com/docs/git-commit)
* [Git add manual page](https://git-scm.com/docs/git-add)
* [Git rm manual page](https://git-scm.com/docs/git-rm)
* [Git mv manual page](https://git-scm.com/docs/git-mv)

### What is a commit?

The version history in Git is made out of commits. A commit is a
collection of changes to files. When it comes to commits a change
refers to the whole state of the whole repository since last
commit. At it's simplest a change in the repository is simply a change
in one of the files contained in the repository. Another type of an
change is the addition of a completely new file, but as one might have
guessed a deletion of an existing file is also a change.

When ever you want to check the state of your repository you can use
the handy status command git offers. For example in some repository we
could have the following state:

~~~ {.bash}
git status

#On branch NewBranchForTesting
#Changes not staged for commit:
#  (use "git add/rm <file>..." to update what will be committed)
#  (use "git checkout -- <file>..." to discard changes in working directory)
#
#        modified:   CHANGELOG.md
#        deleted:    package.json
#
#Untracked files:
#  (use "git add <file>..." to include in what will be committed)
#
#        src/css/mobile.css
#
#no changes added to commit (use "git add" and/or "git commit -a")
~~~

See how '`git status`{.bash}' tells you all kinds of useful
information. You can always run the status command and get all sorts
of useful tips out git, like a few commands that people usually
execute when they end up in the specific state.

Another useful command one might make use of before committing changes
to the repository is the '`git diff`{.bash}' command, if run without
any parameters it simply runs the command for every single file that
has been changed somehow. The diff command shows the difference
between two versions of the file.  Example:

~~~ {.bash}
git diff CHANGELOG.md
~~~

~~~ {.diff}
diff --git a/CHANGELOG.md b/CHANGELOG.md
index 48a95c9..c71034f 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -2,5 +2,12 @@
 I will be making my 1st commit with these changes.
 This is really fun ^_^
 
-#vERSION 2
+#VERSION 2
 Testi
+
+# Version 3
+A huge update
+
+* A complete rewrite of the codebase
+* Some API changes
+* A lot of spelling errors fixed in the user interface
~~~

### Making an actual commit

In order to actually record your changes to version history, you have
to create a commit that contains them. Creating commits include 3
important commands *add*: to include new or modified files to the
commit, *rm*: to remove an existing file and *mv*: to rename or move
an existing file.

1. Add and remove any files that you want to include in the commit,
   with the *add* and *rm* commands.

2. Create the actual commit with the '`git commit`' command. This
   command opens up a text editor, in Windows it is by default
   Notepad, but you can change that if you so wish. In this file you
   need to enter a short headline for your commit on the 1st line, and
   starting from the 3rd line you can write a more detail description
   of the changes you have made. There is no length limit for the
   commit message. Note that any lines starting with # are treated as
   comments and not included in the actual commit message. To finish
   editing the commit message save the file, but don't change the
   filename, and close the editor.

~~~ {.bash}
git add CHANGELOG.md
git add src/css/mobile.css
git rm package.json

 # It is not enough that you remove a file physically from your
 # computer you also need to explicitly tell git that you want to
 # remove that file from the repository too, else git assumes that you
 # have mistakenly removed the file and restores it for you.

git status

 #On branch NewBranchForTesting
 #Changes to be committed:
 #  (use "git reset HEAD <file>..." to unstage)
 #
 #        modified:   CHANGELOG.md
 #        deleted:    package.json
 #        new file:   src/css/mobile.css

git commit
 # Text editor opens up and can write your commit message.

 #[NewBranchForTesting a171f20] This is the headline of my new commit
 # 3 files changed, 11 insertions(+), 45 deletions(-)
 # delete mode 100644 package.json
 # create mode 100644 src/css/mobile.css
~~~


### Things to remember when committing

The rule number one is __never commit binary files__ and I really mean
it, Git doesn't like binary files it has no idea what to do with
them. You can include them in the repository, but if you can avoid it
the do so. If for example png icon files are needed, then it is OK to
commit and include them in the repository, but one should try to find
a text based alternative. SVG would be a good alternative for icon
files, they also look much better on that super duper fancy Apple
Retina screen.

Second rule is to use good and descriptive commit messages. Sure if
you do a one-liner change then it usually does not make any sense to
include a long description in the message. In these cases it is OK to
use only the commit message headline.

Keep your commit logical, don't include two completely different things
in the same commit. Instead split it down to 2 commits. If you have
made several different changes to the same file and have forgotten to
commit the individual changes you can use the
[interactive staging mode](https://git-scm.com/book/en/v2/Git-Tools-Interactive-Staging)
'`git add -i`{.bash}' to select only part of the file for inclusion in
the commit..

Final rule no commit should ever break the code. You should always do
commits in such a way that no matter which version of the app you take
from the history, it will work. It can have bugs, but it's
functionality should never be worse than what it was in the past
commit.


\clearpage

## Preparing your finished feature branch for inclusion

To find out more about the topics covered in this section please refer
to the following webpages.

* [Pro Git Branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
* [Pro Git - 7.6 Git Tools - Rewriting history](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)
* [Git rebase manual page](https://git-scm.com/docs/git-rebase)
* [Git merge manual page](https://git-scm.com/docs/git-merge)

So far you have been working on your own private copy of the code and
you have now, after long tedious work finally finished the feature you
were working on. Now is the time to share the fruits of your work with
other team members. Before your work can be included in the main
repository your version history has to be tidied up. Take look at the
graphs below, the 1st one represents the official repository and the
one below it represents yours.

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 2em]{
Commit A -- Commit B -- Commit C -- Commit D -- Commit E};

\gitbranch
{official/master}
{above=of Commit E}
{Commit E}

\end{tikzpicture}
\caption{Official repository state}
\end{figure}

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 2em]{
Commit A -- Commit B -- Commit C -- {
    {[nodes=unreachable] Commit D -- Commit E},
    Commit K1 -- Commit L -- Commit K2
}};

\gitbranch
{origin/master}
{above=of Commit C}
{Commit C}

\gitbranch
{MyFeatureBranch}
{above=of Commit K2}
{Commit K2}

\end{tikzpicture}
\caption{Your repository state}
\end{figure}

As you can see in the picture, there are several things going on in
your repository. First of all you have commits *K1* and *K2*, now *K2*
is clearly a fix for a bug in the code you wrote in *K1*. Surely you
don't want to let other know that you wrote code with a bug in do
you? Well we can fix that later on, but lets get to the second
problem.

Your feature branch is based on the *Commit C*, but some else
has also been working and has already mainlined his code to the
official repository and thus your branch is out-of-date. Current HEAD
in the official repo is *Commit E* if we try to include your feature
branch to the official master branch we might face some problems. For
us to be able to easily include your changes to the official repo, we
first have to rebase your code to the newest version, that is to make
it looked like this:

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 1.0em]{
Commit A -- Commit B -- Commit C -- Commit D --
Commit E -- Commit K1 -- Commit L -- Commit K2
};

\gitbranch
{MyFeatureBranch}
{above=of Commit K2}
{Commit K2}

\end{tikzpicture}
\caption{Rebased MyFeatureBranch}
\end{figure}

Looks much better right? Now we can simply just copy the commits *K1*,
*L* and *K2* to the official master branch.

While we do the rebase we can actually change every single commit in
our branch in any way we want, and as so we can squash the commits
*K1* and *K2* together to form one commit *K*.

\begin{figure}[H]
\begin{tikzpicture}
\gitDAG[grow right sep = 2.0em]{
Commit A -- Commit B -- Commit C -- Commit D -- Commit E -- Commit K -- Commit L
};

\gitbranch
{MyFeatureBranch}
{above=of Commit L}
{Commit L}

\end{tikzpicture}
\caption{Squashed commits}
\end{figure}



### Doing the actual rebase

Rebasing pretty much just means replaying your commits on top of an
different parent, thus making sure the histories of the two are
compatible.

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[rounded corners, >=Stealth]
\graph[layered layout,
    sibling distance=4.2cm,
    level distance=13mm,
    nodes={draw, rectangle, fill=orange!40,
           inner sep=2mm, font=\large},
    edges={line width=0.3mm}]
{
"(user) Fetch official/master"
-> "(user) Start rebase"
-> "(user) Reorder commits for squashing"
-> "(user) Select commits for inclusion"
-> "If commits left"
-> [bend left, edge label=Yes, color=teal] "(git) Try to replay commit"
-> "If error"
-> [edge label=Yes, color=teal] "(user) Resolve conflicts"
-> "If commits left",

{[same layer] "(git) Try to replay commit", "If error", "(user) Resolve conflicts"},

"If error" -> [edge label=No, color=magenta] "If commits left",
"If commits left" -> [bend right, edge label=No, color=magenta] "(git) Finish rebase"
};

\end{tikzpicture}
\end{center}
\caption{Git rebase process}
\end{figure}

#### Fetching latest commits from official The first thing to do

before starting the actual rebasing process is to make sure you have
latest official commit history available locally.

~~~~~ {.bash}
git fetch official
~~~~~


### Selecting and editing commits for the rebase
Now that you have the up-to-date history available and ready to be
used, you can go ahead and start the actual process.

~~~~~ {.bash}
    # If you aren't in your working branch already, then go ahead and
    # do checkout for it:

    git checkout MyBranch

    # And start the actual rebase
    git rebase -i
~~~~~

This opens up a text editor that displays a list of all commits in
your branch. When you begin the commits are listed in reversed order,
with the oldest commit being at the top of the file. Here you can:

* You can reorder the commits: just change the lines around
* Squash multiple commit together: move the commits you want to
  squash to commit *A* below each others and change the command to
  *squash* on them.
* To change the commit message of specific commit change to
  command to *fixup*. If you want to change the files or hunks (parts
  of files) included in the commit you can use the *edit* command.

Example starting situation, take note of how Git is nice and tells
you all the commands you can use as #comments:

~~~~~ {.bash}
    pick 7412607 Prepare animation core
    pick 25a4e73 Initial CSS rules for animations
    pick caf6847 Bind animations to main page
    pick 366940b Bind animations to other pages as well
    pick 7df039a Fix bug in fading animation

    # Rebase ee0a7e2..366940b onto ee0a7e2 (4 command(s))
    #
    # Commands:
    # p, pick = use commit
    # r, reword = use commit, but edit the commit message
    # e, edit = use commit, but stop for amending
    # s, squash = use commit, but meld into previous commit
    # f, fixup = like "squash", but discard this commit's log message
    # x, exec = run command (the rest of the line) using shell
    # d, drop = remove commit
    #
    # These lines can be re-ordered; they are executed from top to bottom.
    #
    # If you remove a line here THAT COMMIT WILL BE LOST.
    #
    # However, if you remove everything, the rebase will be aborted.
    #
    # Note that empty commits are commented out
~~~~~

We can now start working on the commits:

~~~~~ {.bash}
    # Initial state: we can also use a shorthand format for the commands.
    p 7412607 Prepare animation core
    p 25a4e73 Initial CSS rules for animations
    p caf6847 Bind animations to main page
    p 366940b Bind animations to other pages as well
    p 7df039a Fix bug in fading animation

    # Squash the bug-fix to original code commit.
    p 7412607 Prepare animation core
    s 7df039a Fix bug in fading animation
    p 25a4e73 Initial CSS rules for animations
    p caf6847 Bind animations to main page
    p 366940b Bind animations to other pages as well

    # You might just as well squash the bind animation commits.
    p 7412607 Prepare animation core
    s 7df039a Fix bug in fading animation
    p 25a4e73 Initial CSS rules for animations
    p caf6847 Bind animations to main page
    s 366940b Bind animations to other pages as well

    # When you squash multiple commits together Git prompts you to
    # enter a new commit message for the resulting commit. By default
    # it is simply the original messages joined together.
~~~~~

When you are ready save the file and close the editor.

#### Resolving merge conflicts

Now comes the fun part, merging. When you close your editor git starts
the merging process automatically, if everything is OK you don't need
to anything and everything happens automatically. But someone else
might have made changes to the same files as you, and as so the there
might be conflicts in the commit history. If you both have changed the
same line of code, git needs to know which one to use, which one is
better or maybe both of them are needed.

If an conflict error happens git will inform you about it with an
error message. Git is now in merge conflict state. You can find out
more about the conflicting files and the general state of the merge
with the always so handy '`git status`{.bash}' command.

Here is an example conflicts in a file called *animations.js*. Git
adds some information into the file so you can see where the conflict
is taking place and why.

~~~~~ {.javascript .numberLines}
    function spin(object, x, y, z) {
    <<<<<<< HEAD
        // TODO: Add an actual implementation for this function
    =======
        object.rotateAround('x', x);
        object.rotateAround('y', y);
        object.rotateAround('z', z);
    >>>>>>> fe48e44... Add changelog
    };
~~~~~

As might have guessed a conflicting section starts at the line 2 with
`<<<<<<< HEAD`{.diff}, everything between this marker and
`========`{.diff} marker on the line 4 is what is in the official
repository right now. Everything between `=======`{.diff} and `>>>>>>>
XXYYZZW My commit message`{.diff} at the line 8 is the code that you
wrote for the commit *My commit message*.

You now need to decide what parts you want to include in the final
code. In any case you need to remove all 3 conflict marker
files. Every line that you leave in the file will be included in the
finalized commit.

~~~~~ {.javascript}
    function spin(object, x, y, z) {
        object.rotateAround('x', x);
        object.rotateAround('y', y);
        object.rotateAround('z', z);
    };
~~~~~

When you are done save and do the same thing for all conflicting
files. When you are done you need to add or remove them from the
commit.

~~~~~ {.bash}
    git add animations.js

    # If you now run git status it will tell you that all conflicts
    # are fixed and you can continue.
    git status

    #You are currently rebasing branch 'NewFeature' on 'ee0a7e2'.
    #  (all conflicts fixed: run "git rebase --continue")
    #
    #Changes to be committed:
    #  (use "git reset HEAD <file>..." to unstage)
    #
    #      modified:   animations.js

    # You can now continue with the rebasing
    git rebase --continue
~~~~~

When you do a rebase in git it will do the merge step for every single
commit you defined to be included in the final version on step 2.

If you think you have made a mistake somewhere you can use '`git
commit --abort`{.bash}' to revert back to the state you were before
you started the rebase. You may then proceed to start it again from
the beginning.

#### Pushing and opening a pull request

The rebase is now completed and you are nearly ready to open a pull
request for your fancy feature branch. So far all of the commits you
have made are stored only in your own computer, you need push them to
your online repository so that others can have access to them.

~~~~~ {.bash}
    git push -u origin MyFeatureBranch

    # If you have already pushed your commits to your online
    # clone, then the command above fill fail. This is because the
    # histories of the copies are no more compatible. You now have 2
    # options. You can use the --force option with push and overwrite
    # the remote clone's history with yours. In general one should not
    # use --force as it might break other peoples work if they are
    # working on the same branch. This should not happen in our case,
    # but we can play it safe and create a completely new remote
    # branch.

    git push --force -u origin MyFeatureBranch
    # Or
    git push -u origin origin/MyFeatureFinilized MyFeatureBranch
~~~~~

No the only thing left is to go your Bitbucket account page, open the
repository view and click *Create pull request* button from the left
hand panel. Make sure to select the correct branch from your copy.


\clearpage

## Everyday workflow summarized

1.  Starting to work on a new feature or fix:

    ~~~~~ {#ew-new-branch .bash}
    # In master branch
    git checkout master
    git pull official master
    git branch MyFeatureBranch
    git checkout MyFeatureBranch
    ~~~~~

2.  Committing changes to your feature branch:

    ~~~~~ {#ew-commit .bash}
    git add src/js/NewJsFile.js src/css/FileWithChanges.css
    git rm src/js/UnusedFile.js

    git commit
    # Enter a title for your commit to the 1st line,
    # and a longer description starting at line 3.
    ~~~~~

3.  Preparing your branch for inclusion to official/master:

    ~~~~~ {#ew-prepare .bash}
    git fetch official
    git rebase -i official/master
    # Organize your commits to few small logical units, squashing
    # all one-liner changes to bigger ones and resolve conflicts

    git push --set-upstream origin MyFeatureBranch
    # Then open a pull request in Bitbucket.
    ~~~~~
