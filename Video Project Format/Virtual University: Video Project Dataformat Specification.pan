% Virtual University: IV-Project specification sheet
% Ammaar Mufti <ammaar.amin@gmail.com>
  Ghassan Abarbou <Abarbou.Ghassan.X@student.uta.fi>
  Jesse Jaara <jaara.jesse.j@student.uta.fi>
  Jussi Karhu <Karhu.Jussi.T@student.uta.fi>
  Sami Voutilainen <hyviamakkaroita@gmail.com>
  Santeri Taskinen <santeri.taskinen@kolumbus.fi>


# Introduction
## Purpose of the document

The purpose of this document, is to try describe the data structures
used for the internal representation of data in the application and
define the structure of export formats for use in viewer applications.
This document shall act as reference for declaration of top-level
classes, that describe the 360°-video project within the application.
The exported formats shall be described in more detail, and must fully
comfort the finalized description provided in the newest version of
this document.

This document uses "PDF-links" to refer to different sections of the
document, that is not all links in this document lead to external websites.

TODO: Add a link, from which the newest version may be retrieved.


## Terms used within this document

* __Interaction point__ or __hot-spot__ refer to an visible or
  invisible point in 3D space, that cause an event to occur, when the
  user somehow activates them.
* __IV-project__ is a shorthand form for _interactive 360°-video
  project_.



# General description of an interactive 360° video project

This chapter describes the individual components, which are needed to
create a complete interactive 360° video project. The general
structure of such project is also described in this chapter. Chapters
[Application internal representation](#application-internal-representation)
and [Export formats](#export-formats) describe implementations of this
structure.

## Components
### Project

For clarity the component will be referred in _italics_ style text,
any other instances of the word project shall refer to a IV-Project
in general.

_Project_ as it's name already states is the top-level component in a
project. It's function is to contain all the information needed to
present the video project to end-users or to re-open the project in an
editor. _Project_ also contains any and all metadata needed for
copyright, indexing purposes and/or any general purpose data someone
might find useful.

#### Properties

The properties of the _project_ component are divided into to 2
primary groups: functional properties that affect the end-user
experience and metadata properties that have no effect on the primary
end-user experience.

* Functional properties:
    * List of end-user visible
      [interaction point marker styles](#interaction-point-marker-style).
    * List of [scenes](#scene) that make up the project.

* Metadata properties:
    * Name
    * Longer description of the project
    * Copyright notice

\clearpage

### Scene

Scene is the biggest individual unit of an IV-Project. A basic scene
consists of 360°-video and a set of
[styled](#interaction-point-marker-style) [content nodes](#content-node).
A scene may also use a static picture instead of an video file, as the
texture for the skydome.

If the IV-project is made up of multiple scenes every scene must be
linked to at least one other scene withing the project, in such a
manner that the scenes form exactly one graph. This is to ensure that
the user does not need to resort to accessing a menu, in order to get
back from the scene. Note that it is OK to implement a menu scene,
where the menu items are implemented as content nodes and the scene
can be accessed from each and every scene trough a content node.

When ever a user moves from one scene to another trough a gateway node
the last scene is passed as a parameter to the new node. This
information can be used to position the camera accordingly after the
transition and to affect the general composition of the scene.

The skydome texture's pixels on the first column map to
[-1,-1,-1]...[1, 1, 1] normalized skydome's vertices along negative
side of the z-axis at x-position 0.

#### Properties

* Alphanumeric ID that is unique across all components within the
  _project_.
* Human readable name, does not have to be unique, but should.
* Optional textual description of the scene and it's contents.
* Skydome texture: 360° video file h.264 in mp4 container, a static
  png, jpeg or psychedelic fragment shader in Open GL ES Shading
  Language for menu scenes.
* List of content nodes contained within the scene.


#### Value ranges

| Property               | Minimum | Maximum | Unit   |
|:-----------------------|:-------:|:-------:|:-------|
| skydome texture width  |  2048   |  4096   | pixels |
| skydome texture height |  2048   |  4096   | pixels |

Table: Scene's properties' minimun and maximun values \label{table:minmaxcnode}

\clearpage

### Content node / interaction point

Content nodes are divided in two primary categories, static nodes that
do not react to users actions and interactive nodes that can be
activated by the user. Static nodes are added to scenes to bring in
more content like informative texts and symbols or to create deeper
ambient soundscape.

Content nodes define the type of an action that can happen and any
parameters the action might need. Content nodes them self do not
define any kind of an visual representation for the node, although
certain nodes have a recommended default representation style. A node
may also be left invisible, but then it should be a static node as not
to confuse the user.

Different types of nodes are looked into in more depth in the
[subtypes](#cns) chapter.


#### Properties

All nodes posses the following properties

* Alphanumeric ID that is unique across all components within the
  _project_.
* Human readable name, does not have to be unique, but should.
* Type: one of the [subtypes](#cns).
* Position defines as a azimuth and a zenith value relative to
  skydome, a third value on the scale [0,1] is used to determine
  distance from the surface of the skydome. The center of the physical
  marker representation will reside at the 3D coordinates derived from
  the given values.
* Marker style.
* Parameters to the style: Transformation matrix, textures, uniforms
  and so on.
* Activation zone defined as 2 position coordinate pairs on the
  skydome sphere's surface. Point _A_ detonating the upper left corner
  of the are and point _B_ refering to the lower right corner. If
  azimuthB - azimuthA > 180° the area shall be interpreted as the
  smaller are between the 2 points, that is from point B° the area
  shall be interpreted as the smaller are between the 2 points, that
  is from point B↦A instead of A↦B.
* Activation time.

#### Subtypes {#cns}

The following types of content nodes are recognized.


##### Static node

These nodes simply add more content to the scene than what is visible
in the skydome. Static nodes should always be visible and have a
marker style set to them.

Static nodes may be represented with simple descriptive 3D icons,
signposts, user defined 3D models or floating 3D text.


##### Gateway node

Gateway nodes act as access points between scenes. They should be
represented by the arrow marker style. Gateway nodes also contain one
additional property target scene.


##### Sound node

A sound node represents an action that plays an audio file. The audio
content is mapped to 3D space and will be mixed accordingly in the
player application.

Sound nodes have several additional properties:

* Autoplay
* Loop
* Volume

The autoplay and loop properties can be used to add ambient sounds to
scene, in these cases the sound node should not have a marker style
defined for it. Default and recommended marker style for a user
activatable sound node is the 3D speaker model.


##### Textual node / window node

When activated textual nodes popup 3D surface with a text rendered on
it. The associated text may be formatted with light markup. Text
frames can contain "buttons" that can be activated with gaze, thus
allowing one to navigate between multiple text frames.

Textual nodes include the following additional properties:

* List of text frames, with the following properties:
    * Actual content in markdown.
    * List of buttons, with text and a target frame.


#### Value ranges

| Property            | Minimum | Maximum | Unit           |
|:--------------------|:-------:|:-------:|:---------------|
| azimuth             |   0.0   |  360.0  | degrees        |
| zenith              | -90.0   |  180.0  | degrees        |
| distance            |   0.0   |    1.0  | decimal number |
| activation time     |   0.0   |    5.0  | second         |
| volume              |   0.0   |  100.0  | decimal number |

Table: Content node properties' minimun and maximun values \label{table:minmaxcnode}


\clearpage

### Interaction point marker style

The marker styles are tightly integrated together with the content
nodes, in an final implementation of this general project structure
one does not need to decouple these. But for clarity they are handled
independently.

All markers are represented with 3D models. A predefined set of
suggested models will be provided by the editor, but a user may opt-to
create his own textures, shaders and/or models for the content nodes.

Unlike content nodes the substyles do not have different functionality,
but simply have some minor differences on how the textures are mapped
and what properties the editor application will allow the creator to set.

Each and every content node has it's own marker style, but any common
assets may be shared across them.

#### Properties

All marker styles contain the following properties:

* Associated 3D model.
* Transformation matrix with rotation and scaling defined in it.
* Vector and fragment shader. See [shaders](#shaders) for more
  information.
* Set of UV mapped textures, with slots for the primary texture,
  normal map and specular map. ETC1 in ktx container or PNG, PNG is
  only recommended for normal maps, all other textures should be in
  ETC1. See [textures](#textures) for more information.

\clearpage

#### Shaders

A marker style may contain exactly one vertex and/or fragment
shader. Shaders must be fully compatible with WebGL level shader
language.

##### Vertex shaders

The table \ref{table:vxunis} below lists and describes all of the
vertex shader uniforms that are guaranteed to be always
available. They are the same as provided by Three.js, with one
additional for implementing activation progress animations.
Also the following attributes described in table \ref{table:vxattrs}
are always guaranteed to be available.

| GLSL type | Name             | Short Description                    |
|:---------:|:-----------------|:-------------------------------------|
|   mat4    | projectionMatrix | Camera's projection matrix           |
|   mat4    | viewMatrix       | Camera's world transformation matrix |
|   vec3    | cameraPosition   | Camera's world location              |
|   mat4    | modelMatrix      | Model's world transformation matrix  |
|   mat4    | modelViewMatrix  | viewMatrix * modelMatrix             |
|   mat3    | normalMatrix     | Inverse transpose of modelViewMatrix |
|   float   | mProgress        | Activation progress                  |
|   float   | mVideoProgress   | Video position in milliseconds       |
|   float   | mVideoLength     | Video's total length in milliseconds |


Table: Default vertex shader uniforms \label{table:vxunis}

Additional uniforms can also be provided for custom shaders, the total
amount of vertex uniforms should not exceed 128 vector components to guarantee
compatibility with all devices. Also all uniform values have to be
static and defined per content node. If dynamicity is needed, the value
must be calculated in the shader with the help of mProgress and
mVideoProgress uniform values.


| GLSL type | Name     | Short description          |
|:---------:|:---------|:---------------------------|
|   vec3    | positon  | Coordinates of the vertex  |
|   vec3    | normal   | Averaged from face normals |
|   vec2    | uv       | UV texture coordinates     |

Table: Default vertex shader attributes \label{table:vxattrs}

As for vertex attributes up to 16 attributes can be defined in total,
that is including the default attributes. As for uniforms all custom
attributes must be static and defined per content node. Any dynamic
values must be calculated with the help of the progress uniforms.

\clearpage

##### Fragment shaders

The table \ref{table:fragunis} describes the default uniforms
guaranteed to be available in every fragment shader.

| GLSL type | Name             | Short description                    |
|:---------:|:-----------------|:-------------------------------------|
|   mat4    | viewMatrix       | Camera's world transformation matrix |
|   vec3    | cameraPosition   | Camera's world location              |
|   float   | mProgress        | Activation progress                  |
|   float   | mVideoProgress   | Video position in milliseconds       |
|   float   | mVideoLength     | Video's total length in milliseconds |

Table: Default fragment shader uniforms \label{table:fragunis}

As with vertex shaders user may define custom fragment shader
uniforms, with static values defined per content node. A maximum of 64
vectors of uniforms can be defined. Animation can implemented with the
progress uniforms.


##### Cube camera

In order to support reflections and more fancier effects, needed by
the default gateway arrow symbol a "cube camera" is required in the
scene. That is the scene rendered to an OpenGL samplerCube. Resolution
does not matter so much for the cube camera. Also it is acceptable to
provide only one cube camera positioned in the center of the sphere to
save resources.

More info:
* [OpenGL wiki's Framebuffer object examples page](https://www.opengl.org/wiki/Framebuffer_Object_Examples#Quick_example.2C_render_to_texture_.28Cubemap.29)
* [opengl-tutorial.org – Tutorial 14: Render to texture](http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/)


#### Textures

Default shaders are provided for textures that consist of actual
texture, normal and specular maps. An individual texture file should
not be larger than 2048x2048 pixels to conserve memory on handheld
devices. All textures should be provided in ETC1 encoded form and
stored in Khronos' ktx container, normal maps or other textures that
would suffer greatly from ETC1 compression artifacts may be provided
in PNG format.

If more complex texturing is needed a custom set of shaders must be
provided with the texture files provided as uniform values.


#### Value ranges

| Property       | Minimum |   Maximum    | Unit          |
|:---------------|:-------:|:------------:|:--------------|
| texture width  |   256   |    2048      | pixels        |
| texture height |   256   |    2048      | pixels        |
| mProgress      |   0.0   |     1.0      | decimal value |
| mVideoLength   |   0.0   |      ∞       | seconds       |
| mVideoProgress |   0.0   | mVideoLength | seconds       |

Table: Minimum and maximum values for content node style properties \label{table:minmaxstyle}



# Export format
## Compiled end user package

The main output format of the application is a compiled zip-based
file, which includes all videos and extra assets together descriptor
files. Video files are not compressed, to allow streaming directly
from the within the zip file.

> Zip files allow one to store the individual files with different
> compression settings, including archive mode (no compression).

### Example project tree structure

\dirtree{%
.1 Hello project.zip.
.2 Hello project.
%
.3 assets.
.4 models.
.5 Arrow.obj.
.5 Hello World text.obj.
.5 Speaker.obj.
%
.4 textures.
.5 Speaker.ktx.
.5 Speaker-normal.png.
.5 Speaker-specular.ktx.
%
.4 images.
.5 logo.png.
%
.4 shaders.
.5 vertex.
.6 Animated-Arrow-vertex.glsl.
.6 Phong-vertex.glsl.
.5 fragment.
.6 Animated-Arrow-frag.glsl.
.6 Phong-frag.glsl.
%
.4 skydomes.
.5 Hello Park.mp4.
.5 Hello road.jpeg.
%
.3 project.json.
}


\clearpage

## JSON-based project description

A folder structure shall contain a single descriptor file named
'__project.json__' in the root folder.

Each of the following sub-chapters shall give an fully qualified
example snippet and explain the individual fields.

Most components contain an _id_-attribute that is
[UUID V4](https://www.wikiwand.com/en/Universally_unique_identifier)
formatted string, description of these attributes will be omitted in
the following chapters. _Name_ and _description_ are also present in
most components and the same meaning everywhere. _Name_ is a human
readable name/identifier for the specific instance of the component,
primarily for the needs of editor applications. _Description_ can be
left empty and is used for longer descriptions of the instance.


### Top-level/Metadata

Project.json file starts with a few metadata headers. Information
contained can be formatted in any way.

~~~~~{.json}
{
  "fmtvers"    : "0.9.0",
  "name"       : "East end area of Hämeenkatu",
  "description": "The purpose of this test project is to test how this system works.",
  "location"   : "Tampere",
  "copyright"  : "The hello project is free for private use",
  "author"     : "Jesse Jaara <jjj@gmail.com>, Santeri Taskinen <taskinen.sanetri@foo.bar>",
  "mainScene"  : "ID",
  "scenes"     : [objects]
}
~~~~~

fmtvers (String):
: Fileformat' version. __Not project's version__. Follows the rules of
  [Semantic Versioning 2.0](http://semver.org/). Current version is
  0.9.0 for now. Will be bumped up to 1.0.0, when the specification is
  fully implemented in the editor.

location (String):
: Where are the scenes located in general. Formatting is free.

copyright (String):
: Copyright holders and redistribution information.

author (String):
: A freely formatted list of authors.

mainScene (String):
: ID of the 1st scene that is to be loaded.

scenes (Array<object>):
: An array of the scene-objects that make up the project.

### Scenes

~~~~~{.json}
[
  {
    "name"       : "Stockmann scene",
    "id"         : "2e69c306-6493-404c-9a63-0eabc6164a3d",
    "description": "Traffic in front of Stockmann emporium.",
    "skydome"    : {object},
    "nodes"      : [objects]
  }, {...}
]
~~~~~

skydome (object):
: Defines the image/video/texture that is to be displayed to the user.

nodes (Array<object>):
: List of interactive nodes present in the scene.

\clearpage

### Skydome

~~~~~{.json}
{
  "id": "a0bf366b-782c-4e6b-9a51-8d07a716f9cf",
  "name": "Stockmann skydome",
  "description": "",
  "type": "video",
  "file": "/assets/skydomes/stockmann.mp4"
}
~~~~~

type (String):
: May be one of _image_, _video_ or [_texture_](#texture).

file (String):
: Fully qualified and direct path the resource file. Root is assumed
  to be folder where project.json resides. Only provided for _image_
  and _video_ types.

texture (object):
: If type is _texture_ a texture component, as described in the
 texture chapter later, has to be provided.


### Texture

~~~~~{.json}
{
  "vertexShader"  : "/assets/shaders/vertex/Example-vertex.glsl",
  "fragmentShader": "/assets/shaders/fragment/Example-fragment.glsl",
  "uniforms"      : [
    {"name": "mRefractionRatio", "type": "f",      "value": 1.02},
    {"name": "tCube",            "type": "camera", "value": null},
    {"name": "mActivatedColor",  "type": "c",      "value": [0.0, 1.0, 0.0]},
    {"name": "myMatrix",         "type": "m3",     "value": [1.1, 1.2, 1.3,
                                                             2.1, 2.2, 2.3,
                                                             3.1, 3.2, 3.3]},
    {"name": "mDoge",            "type": "img",    "value": "/assets/textures/doge.ktx"}
  ]
}
~~~~~

vertexShader / fragmentShader (String):
: A path to OpenGL ES shader code file.

uniforms (Object):
: List of custom uniforms to pass to the shader programs.

> In the example matrix, the integer part detonates the column and the
> fractional part tells the row. So it will be interpreted as the
> transpose of itself if laid out like in the example.

#### Uniforms

All uniforms have a _name_ that is same as used in the shader code, a
_type_ and a _value_.

| Type String |  GLSL type  | JSON representation | Note                             |
|:-----------:|:-----------:|:-------------------:|:---------------------------------|
|     'f'     |    float    |        Number       |                                  |
|     'v2'    |    vec2     |     Array<Number>   |                                  |
|     'v3'    |    vec3     |     Array<Number>   |                                  |
|     'c'     |    vec3     |     Array<Number>   | RGB color values in range [0, 1] |
|     'v4'    |    vec4     |     Array<Number>   |                                  |
|     'm3'    |    mat3     |     Array<Number>   | Column major list of values      |
|     'm4'    |    mat4     |     Array<Number>   | Column major list of values      |
|    'img'    |  sampler2D  |        String       | File path                        |
|   'camera'  | samplerCube |         null        | See [Cube camera](#cubecamera)   |

Table: Mappings between JSON and GLSL value types

\clearpage

### Nodes

~~~~~{.json}
[
  {
    "id"            : "51cc5d44-8718-4b3f-bac1-be2be9bcda13",
    "name"          : "Railway station gateway",
    "description"   : "",
    "type"          : "gateway",
    "target"        : "ca78de61-62eb-4340-a414-c2b5264c1c4a",
    "position"      : [10.0, -10.0, -70.0],
    "rotation"      : [170.0, 0.0, 0.0],
    "scale"         : [0.5, 0.5, 0.5],
    "model"         : "/assets/models/arrow.json",
    "texture"       : {object},
    "activationTime": 5.0,
    "activationArea": {"ul": [1.0, 0, 30.0], "lr": [1.0, -15.0, 350.0]}
  }, {
    ...,
    "type"    : "audio",
    "file"    : "/assets/audio/rain.m4a",
    "loop"    : false,
    "autoplay": false,
    ...
  }, {
    ...,
    "frames"     : [objects],
    "firstFrame" : "aa75de61-68ec-4341-a414-c2b5287c1c4a"
  }
]
~~~~~

type (String):
: One of _static_, _gateway_, _audio_, _textual_.

target (String):
: ID of the targetr scene

position (Array<Number>):
: Node's position in spherical coordinates
  [distance from origin, zenith, azimuth].

rotation (Array<Number>):
: Rotation of the 3D mesh in Euler angles in x->y->z order [x, y, z].

scale (Array<Number>):
: Scaling factor for the 3D mesh [x, y, z].

model (String):
: Path to the 3D mesh model used to visualize the node. Maybe empty
  for audio nodes.

texture (object):
: Texture used on the 3D mesh. See [Texture](#texture)

activationTime (Number):
: Time in seconds, needed to activate the node

activationArea (Object):

: Spherical coordinate on the skydome sphere. Ul = Upper left corner,
  lr = lower right corner. [1.0, zenith, azimuth]

file (String):
: A path to the audio file.

loop (Boolean):
: Should the audio be looped.

autoplay (Boolean):
: If true, playback begins as soon as the scene has loaded.

frames (Array<Object>):
: An array of text frame objects.

firstFrame (String): 
: The ID of the 1st text frame.

\clearpage

#### Text frame

~~~~~{.json}
{
  "id"       : "c84c1452-334c-497d-89c6-f237128c114d",
  "headline" : "Markdown!",
  "text"     : "## Level 2 header\n\n Goes **here**.![image](/assets/images/logo.png)",
  "links"    : [
    { "name": "Image", "target": "446b7cf3-7e93-40e6-bed6-625cfa86b557" },
    { ... }
  ]
}
~~~~~

text (String):
: Markdown formatted text content of the frame.

links (Array<Object{String, String}>):
: Link to another text frame beloging to the same cnode. _Name_ is the
  text the user will see in the application as a button. _Target_ is
  the id of the target frame.


### Revision history

0.9.0:
: Initial public specification.

0.10.0:
: JSON specification for textual nodes
