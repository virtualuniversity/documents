% IVEditor manual
% Jesse Jaara <jesse.jaara@gmail.com>

# System requirements

Powerful CPU and at least Intel HD 4400 or better graphics card. You
will also need Firefox 43 or newer or Chromium (Google Chrome) 47 or
newer to run the editor.


# Getting and preparing the IVEditor for use
## Windows

1. Download the newest version of the Windows package from
   <https://bitbucket.org/virtualuniversity/iv360-edit/downloads>

2. Extract the zip file to somewhere on your computer.

3. Double click the IVEditor.exe file in your to start the IVEditor
   server.

## Linux and OS X

1. Make sure you have git and atleast version 5.4.0 of Node.js
   installed on your system. Refer to google for instructions.

2. Open your terminal application of choise and change to a directory
   you want to "install" IVEditor into.

3.  Run following commands:

~~~~~{.bash}
git clone
https://Huulivoide-UTA@bitbucket.org/virtualuniversity/iv360-edit.git IVEditor
cd IVEditor
npm install
node bin/install_dependencies.js

# And to start the application server
node IVEditor.js
~~~~~


# Operating the editor
## Starting a new project

The editor does not support creating completely new project from
scratch as of yet, so you need to download the project stub from
the application repository and extract it to somewhere on your
computer.
<https://bitbucket.org/virtualuniversity/iv360-edit/downloads/project-stub.zip>


## Editing an existing project

![Browser window](pictures/browser.png)

When you started the server your browser should have started opened up
a a web page <http://localhost:8000/browser>. If the link opened up in
a wrong browser copy this link and open it in Firefox or Chromium.

The browser page lets you to browse your computers file system. If you
are running IVEditor on windows you can only access the contents of
the C-drive.

![Navigating trough the file system](pictures/project.json.png)

To open a project for editing navigate trough the directories to where
you have an existing project or have extracted the project stub. Open
the project directory and you should see the browser display a file
named 'project.json'. The browser only shows you directories and these
special IVEditor project files. To edit the project click the project
file and select 'Edit' from the window that opens.

![Project action dialog. Show or edit an existing project](pictures/EditShow.png)

\clearpage


## Tool bars

![Scene and node list](pictures/list.png)

This is one of the 2 main control panels of the editor.

1. Name of the project. By clicking this you can edit the project's
   information, like author and location in the second tool box.

2. Button that creates a new scene.

3. List of scenes. Each scene has a submenu of nodes included in
   them. But only the nodes of currently active scene are shown. By
   clicking a scene in the Scenes and Nodes tool box the editor loads
   up the scene for viewing and sets the Data toolbox into Scene
   editing mode. You can remove the node and all associated content
   nodes by clicking the red litter bin icon.

4. Clicking a content node in the list will turn camera to face that
   node and sets the data toolbox into node editing mode. Like scenes
   you can remove individual nodes bu clicking the litter bin icon.

5. Takes you back to the browser.

6. Exports the project and takes the webbrowser into the viewer mode.

7. Export all assets and project info the the project folder, that is
   save the project.

8. Exports the project and writes all it's files into a portable zip
   that can be played by third party viewing apps.

\clearpage

![Data toolbox – Project mode](pictures/project.png)

No matter what you have selected project, scene or a content node, the
data toolbox will always show fields for name and description of the
object.

In the Project mode it will also display fields for the general
location on of the scenes on planet Earth or any other planet or galaxy
for that matter. A copyright field where you can also include some
licensing and copying info about the project. And finally the author
of the project.

![Data toolbox – Scene mode](pictures/scenemode.png)

When you have an scene selected in the list toolbox the data toolbox
gives you and option to change the video or image that is to be used
the skydome texture for the scene.

\clearpage


## Editing content nodes

![Data toolbox – Content node mode](pictures/controlpanel_all_open.png)

\clearpage

In the Content node mode the data toolbar will always show controls
for:

* 3. Type:
    - static: Just sits in the scene, and newer activates anything.
    - audio: Plays sounds when activated
    - text: Displays a text frame when activated
    - gateway: Takes you to places.

* 5. Model:
    - Default arrow: Default for gateway nodes.
    - Default speaker: Default for audio nodes.
    - Font Awesome Symbol: Opens a dialog where you can enter hex code
      of an Font Awesome icon to generate 3D model from it.
    - List of use defined 3D models.

* 6. Texture: The visual look of the 3D model
    - fresnel and color textures provide 2 colors that you user can
      change, 1st color mDefaultColor in fresnel shader is the color
      that non activated nodes have. The 2nd color defines the color
      that the node turns into when it is being activated. Fresnel
      reflects and shines the enviroinment around it, while color is
      matte.
    - simple and phong lets you use image as a texture, phong also
      allows you to set normal and specular maps for the texture.

* Position:
    - 16. radius: The distance from the viewer
    - 17. zenith: Moves the node up and down.
    - 18. azimuth: Moves the node left and right

* rotation: 19-21. Euler rotation in x→y→z order.

* scale: 22. Makes the object bigger and smaller.

* Activation are: lets you change the size of the are that is
  considered to be part of the node when activating nodes in the
  viewer applications. Activation area is defined relative to the
  position of the node it self. up, down, left and right in
  degrees. Also a check box is poroved to turn on and off the
  visualization of the area, this is only for the editor and it will
  not reflect the exported project in any way.

* activation time: 23. Time in seconds needed to activate the node.

## Content node type specific options

Each content node type will have it's type specific options shown
below the Type option in the data toolbar.

Gateway nodes will have a target option that displays a dropdown list
of scenes in the project.

Audio nodes will have checkboxes for autoplay and looping, volume
control and a dropdown list of usable audio files.

Text nodes have a button to open a frame editor.

\clearpage

## Editing text nodes

![Text frame editor](pictures/frameeditor.png)

Clicking the 'Edit Text Node' menu item in the data toolbox's type
specific menu will bring up the text frame editing menu.

Each text node is made up of multiple or single interlinked
frames. Frames can branch out as much as you want, but you should not
branch them back in and under no circumstances should you ever create
a cyclic graph out of them.

1. Is a tab bar, like the in your browser. It shows you the frames
   that are contained in the node. Like everywhere else, you can
   remove a frame by clicking the red litter bin icon. Currently
   active frame is displayed in green, and the main frame, when not
   active will be displayed in blue.

2. Here you can enter the headline of this particular frame.

3. The main content goes in here. You can use full standard Markdown
   syntax as per the [CommonMark Spec](http://spec.commonmark.org/)
   with one exception. All links to outside websites are forbidden,
   and shall not be supported in any manner. What comes to images
   their paths must be in the format `/assets/images/myimage.png`.

4. Connections between frames are managed trough so called links. In
   the official web viewer application these are represented by
   buttons at the bottom part of the frame. Each link has a name and a
   target scene. You can remove existing links with red icon and add
   new ones with the 'Add link +' button.

When you are done editing just click the 'X' button at the top right
corner of the dialog.

\clearpage


# Using the viewer

Open the browser window and navigate to the project folder of the
project you want to view. Like with editing click the project.json
file and choose __view__ from the dialog that opens.

To spin around the image double click anywhere on the page. Your
browser will tell you that the page wanst to grab your mouse, click
the OK button or equivalent on that dialog.

To exit the Pointer Lock mode at any time just press the __Esc__ key
on your keyboard.

