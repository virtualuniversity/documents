#!/usr/bin/bash

DOCUMENT="IVEditor manual"

FORMATTERS=('fancy_lists'
            'definition_lists'
            'superscript'
            'subscript'
            'pipe_tables'
            'raw_tex'
            'fenced_code_blocks'
            'fenced_code_attributes'
            'backtick_code_blocks'
            'inline_code_attributes')

FORMATTER_STR=""
for format in "${FORMATTERS[@]}"; do
    FORMATTER_STR="${FORMATTER_STR}+${format}"
done

rm -f date.stamp
git show -s --format=%ci > date.stamp

pandoc -V papersize:a4paper \
       -V geometry:margin=2cm \
       -V mainfont:"Noto Serif" \
       -V sansfont:"Noto Sans" \
       -V monofont:"Liberation Mono" \
       -V documentclass:scrreprt \
       -V lang:english \
       --toc \
       --toc-depth=6 \
       -f markdown${FORMATTER_STR} \
       --latex-engine=xelatex \
       --template=DocumentTemplate.tex \
       -o "${DOCUMENT}.pdf" \
       "${DOCUMENT}.pan"

